package com.naumen.test.app

import android.app.Application
import com.naumen.test.di.AppComponent
import com.naumen.test.di.DaggerAppComponent
import com.naumen.test.di.modules.ContextModule
import com.nostra13.universalimageloader.core.DisplayImageOptions
import com.nostra13.universalimageloader.core.ImageLoader
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration


/**
 * Created by Pavel Votyakov on 03.05.17.
 * Work in progress © 2017
 */

class ComputersApp : Application() {

    override fun onCreate() {
        super.onCreate()
        appComponent = DaggerAppComponent
                .builder()
                .contextModule(ContextModule(this)).build()

        ImageLoader.getInstance().init(ImageLoaderConfiguration.Builder(this)
                .defaultDisplayImageOptions(DisplayImageOptions.Builder()
                        .cacheOnDisk(true)
                        .cacheInMemory(true)
                        .build()).build())
    }

    companion object {
        lateinit var appComponent: AppComponent
    }
}
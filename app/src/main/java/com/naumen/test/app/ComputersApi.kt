package com.naumen.test.app

import com.naumen.test.domain.models.NWComputer
import com.naumen.test.domain.models.NWComputersResponse
import com.naumen.test.domain.models.NWDetailComputer
import io.reactivex.Maybe
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

/**
 * Created by Pavel Votyakov on 03.05.17.
 * Work in progress © 2017
 */

val SERVER_NAME = "http://testwork.nsd.naumen.ru/rest/"

interface ComputersApi {

    @GET("computers")
    fun getComputers(@Query("p") page: Int): Maybe<NWComputersResponse>

    @GET("computers/{id}")
    fun getComputerById(@Path("id") id: Int): Maybe<NWDetailComputer>

    @GET("computers/{id}/similar")
    fun getSimilars(@Path("id") id: Int): Maybe<Collection<NWComputer>>


}
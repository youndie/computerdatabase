package com.naumen.test.presentation.presenter

import com.arellomobile.mvp.InjectViewState
import com.naumen.test.app.ComputersApp
import com.naumen.test.app.DETAIL_SCREEN
import com.naumen.test.domain.repository.ComputersRepository
import com.naumen.test.domain.repository.ToolbarTitleRepository
import com.naumen.test.presentation.transformer.DBToUIDetailComputerTransformer
import com.naumen.test.presentation.transformer.NWToUIComputerTransformer
import com.naumen.test.presentation.view.DetailView
import ru.terrakok.cicerone.Router
import javax.inject.Inject

@InjectViewState
class DetailPresenter(val computerId: Int) : BasePresenter<DetailView>() {

    @Inject
    lateinit var computersRepository: ComputersRepository
    @Inject
    lateinit var router: Router

    init {
        ComputersApp.appComponent.inject(this)
        loadComputer()
    }

    var startedLoadingSimilar = false

    fun loadSimilars() {
        if (startedLoadingSimilar) return
        startedLoadingSimilar = true
        computersRepository.getSimilars(computerId).compose(NWToUIComputerTransformer).subscribe({
            viewState.finishLoadingSimilars()
            viewState.showSimilars(it)
            startedLoadingSimilar = false
        }, {
            viewState.finishLoadingSimilars()
            startedLoadingSimilar = false
        })

    }

    fun loadComputer() {
        computersRepository.getDetailComputer(computerId).compose(DBToUIDetailComputerTransformer).subscribe({
            viewState.showDetailComputer(it)
        }, {
            viewState.showLoadingError()
        })
    }

    fun similarClicked(id: Int) = router.navigateTo(DETAIL_SCREEN, id)

}

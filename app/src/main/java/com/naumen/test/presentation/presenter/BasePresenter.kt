package com.naumen.test.presentation.presenter

import com.arellomobile.mvp.MvpPresenter
import com.arellomobile.mvp.MvpView
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable


/**
 * Created by Pavel Votyakov on 28.04.17.
 * Work in progress © 2017
 */

open class BasePresenter<View : MvpView> : MvpPresenter<View>() {
    val compositeDisposable = CompositeDisposable()

    fun unsubscribeOnDestroy(disposable: Disposable) {
        compositeDisposable.add(disposable)
    }

    override fun onDestroy() {
        super.onDestroy()
        compositeDisposable.clear()
    }
}


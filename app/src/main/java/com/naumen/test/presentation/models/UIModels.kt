package com.naumen.test.presentation.models

/**
 * Created by Pavel Votyakov on 03.05.17.
 * Work in progress © 2017
 */

open class UIComputer(val id: Int, val name: String, val companyName: String?)

class UIDetailComputer(id: Int, name: String, companyName: String?, val imageUrl: String?, val description: String?) : UIComputer(id, name, companyName)
object UILoader

class UIPageCounter(val pageCount: Int)
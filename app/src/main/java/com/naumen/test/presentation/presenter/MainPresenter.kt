package com.naumen.test.presentation.presenter

import com.arellomobile.mvp.InjectViewState
import com.naumen.test.app.ComputersApp
import com.naumen.test.app.PAGES_SCREEN
import com.naumen.test.domain.repository.ToolbarTitleRepository
import com.naumen.test.presentation.view.MainView
import ru.terrakok.cicerone.commands.Back
import ru.terrakok.cicerone.commands.BackTo
import ru.terrakok.cicerone.commands.Command
import ru.terrakok.cicerone.commands.Forward
import javax.inject.Inject

/**
 * Created by Pavel Votyakov on 05.05.17.
 * Work in progress © 2017
 */
@InjectViewState
class MainPresenter : BasePresenter<MainView>() {

    init {
        ComputersApp.appComponent.inject(this)
    }

    @Inject
    lateinit var toolbarTitleRepository: ToolbarTitleRepository

    override fun onFirstViewAttach() {
        super.onFirstViewAttach()
        unsubscribeOnDestroy(toolbarTitleRepository.subscribe({ viewState.showTitle(it) }))
    }

    private var backStack = 0

    fun onCommand(command: Command) {
        when (command) {
            is Back -> {
                backStack--
            }
            is Forward -> {
                backStack++
            }
            is BackTo -> {
                if (command.screenKey == PAGES_SCREEN)
                    backStack = 0
            }
        }
        toolbarTitleRepository.setTitle(if (this.backStack != 0) "" else "Computer Database")
        viewState.configureToolbar(this.backStack > 0)
    }

    fun resetBackStack() {
        backStack = 0
        viewState.showTitle("Computer Database")
    }
}
package com.naumen.test.presentation.transformer

import com.naumen.test.domain.models.NWComputer
import com.naumen.test.presentation.models.UIComputer
import io.reactivex.Maybe
import io.reactivex.MaybeSource
import io.reactivex.MaybeTransformer

/**
 * Created by Pavel Votyakov on 04.05.17.
 * Work in progress © 2017
 */

object NWToUIComputerTransformer : MaybeTransformer<Iterable<NWComputer>, Iterable<UIComputer>> {
    override fun apply(upstream: Maybe<Iterable<NWComputer>>): MaybeSource<Iterable<UIComputer>> =
            upstream.map {
                it.map {
                    UIComputer(
                            name = it.name,
                            id = it.id,
                            companyName = it.company?.name)
                }
            }

}
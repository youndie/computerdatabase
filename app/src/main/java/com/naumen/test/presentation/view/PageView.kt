package com.naumen.test.presentation.view

import com.arellomobile.mvp.MvpView
import com.naumen.test.presentation.models.UIComputer

interface PageView : MvpView {
    fun showComputers(it: Iterable<UIComputer>)
    fun finishLoading()
    fun startLoading()
}

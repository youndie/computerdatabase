package com.naumen.test.presentation.presenter

import android.graphics.Bitmap
import android.view.View
import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import com.naumen.test.presentation.models.UIDetailComputer
import com.naumen.test.presentation.view.DetailsHeaderView
import com.nostra13.universalimageloader.core.ImageLoader
import com.nostra13.universalimageloader.core.assist.FailReason
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener

/**
 * Created by Pavel Votyakov on 05.05.17.
 * Work in progress © 2017
 */
@InjectViewState
class DetailHeaderPresenter(val uiDetailComputer: UIDetailComputer) : MvpPresenter<DetailsHeaderView>() {

    override fun onFirstViewAttach() {
        super.onFirstViewAttach()
        viewState.showDetailComputer(uiDetailComputer)
        loadPicture()
    }

    fun loadPicture() {
        if (uiDetailComputer.imageUrl == null) {
            viewState.hidePicture()
            viewState.hideProgress()
            return
        }

        ImageLoader.getInstance().loadImage(uiDetailComputer.imageUrl, object : ImageLoadingListener {
            override fun onLoadingComplete(imageUri: String?, view: View?, loadedImage: Bitmap?) {
                viewState.hideProgress()
                viewState.showPicture(loadedImage!!)
            }

            override fun onLoadingStarted(imageUri: String?, view: View?) {
                viewState.showProgress()
            }

            override fun onLoadingCancelled(imageUri: String?, view: View?) {
                viewState.hideProgress()
            }

            override fun onLoadingFailed(imageUri: String?, view: View?, failReason: FailReason?) {
                viewState.hideProgress()
            }
        })
    }

    fun textClicked() {
        viewState.showFullText()
    }


}
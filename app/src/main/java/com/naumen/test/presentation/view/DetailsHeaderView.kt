package com.naumen.test.presentation.view

import android.graphics.Bitmap
import com.arellomobile.mvp.MvpView
import com.naumen.test.presentation.models.UIDetailComputer

/**
 * Created by Pavel Votyakov on 05.05.17.
 * Work in progress © 2017
 */
interface DetailsHeaderView : MvpView {
    fun showFullText()
    fun hidePicture()
    fun hideProgress()
    fun showPicture(loadedImage: Bitmap)
    fun showProgress()
    fun showDetailComputer(uiDetailComputer: UIDetailComputer)
}
package com.naumen.test.presentation.presenter

import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import com.naumen.test.app.ComputersApp
import com.naumen.test.domain.repository.ComputersRepository
import com.naumen.test.presentation.view.PagesView
import javax.inject.Inject

@InjectViewState
class PagesPresenter : MvpPresenter<PagesView>() {

    @Inject
    lateinit var computerRepository: ComputersRepository

    var pagesCount = 0

    init {
        ComputersApp.appComponent.inject(this)
    }

    override fun onFirstViewAttach() {
        super.onFirstViewAttach()
        load()
    }

    fun load() {
        computerRepository.getPages().subscribe({
            viewState.showPages(it)
            pagesCount = it.size
            onCurrentPageChanges(0)
        }, {
            viewState.showError()
        })
    }

    fun onCurrentPageChanges(position: Int) {
        viewState.showPagesText("${position + 1} / ${pagesCount}")
    }

}

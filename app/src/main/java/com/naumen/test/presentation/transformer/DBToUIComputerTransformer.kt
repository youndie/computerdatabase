package com.naumen.test.presentation.transformer

import com.naumen.test.domain.models.DBComputer
import com.naumen.test.presentation.models.UIComputer
import io.reactivex.Maybe
import io.reactivex.MaybeSource
import io.reactivex.MaybeTransformer

/**
 * Created by Pavel Votyakov on 03.05.17.
 * Work in progress © 2017
 */

object DBToUIComputerTransformer : MaybeTransformer<Iterable<DBComputer>, Iterable<UIComputer>> {
    override fun apply(upstream: Maybe<Iterable<DBComputer>>): MaybeSource<Iterable<UIComputer>> =
            upstream.map {
                it.map {
                    UIComputer(
                            name = it.name,
                            id = it.id,
                            companyName = it.company?.name)
                }
            }

}
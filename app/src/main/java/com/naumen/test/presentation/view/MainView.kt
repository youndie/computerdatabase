package com.naumen.test.presentation.view

import com.arellomobile.mvp.MvpView

/**
 * Created by Pavel Votyakov on 05.05.17.
 * Work in progress © 2017
 */
interface MainView : MvpView {
    fun showTitle(title: String)
    fun configureToolbar(backVisible: Boolean)
}
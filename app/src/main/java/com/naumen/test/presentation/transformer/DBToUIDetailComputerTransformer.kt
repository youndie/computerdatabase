package com.naumen.test.presentation.transformer

import com.naumen.test.domain.models.DBDetailComputer
import com.naumen.test.presentation.models.UIDetailComputer
import io.reactivex.Maybe
import io.reactivex.MaybeSource
import io.reactivex.MaybeTransformer

/**
 * Created by Pavel Votyakov on 03.05.17.
 * Work in progress © 2017
 */

object DBToUIDetailComputerTransformer : MaybeTransformer<DBDetailComputer, UIDetailComputer> {
    override fun apply(upstream: Maybe<DBDetailComputer>): MaybeSource<UIDetailComputer> = upstream.map {
        UIDetailComputer(
                id = it.id,
                name = it.name,
                companyName = it.company?.name,
                description = it.description,
                imageUrl = it.imageUrl
        )
    }

}
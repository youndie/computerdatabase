package com.naumen.test.presentation.view

import com.arellomobile.mvp.MvpView
import com.naumen.test.presentation.models.UIComputer
import com.naumen.test.presentation.models.UIDetailComputer

interface DetailView : MvpView {
    fun showDetailComputer(detailComputer: UIDetailComputer)
    fun finishLoadingSimilars()
    fun showSimilars(it: Iterable<UIComputer>)
    fun showLoadingError()

}

package com.naumen.test.presentation.presenter

import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import com.naumen.test.app.ComputersApp
import com.naumen.test.app.DETAIL_SCREEN
import com.naumen.test.common.async
import com.naumen.test.domain.repository.ComputersRepository
import com.naumen.test.presentation.transformer.DBToUIComputerTransformer
import com.naumen.test.presentation.view.PageView
import ru.terrakok.cicerone.Router
import javax.inject.Inject

@InjectViewState
class PagePresenter(val page: Int) : MvpPresenter<PageView>() {

    @Inject
    internal lateinit var computersRepository: ComputersRepository
    @Inject
    lateinit var router: Router

    init {
        ComputersApp.appComponent.inject(this)
    }

    override fun onFirstViewAttach() {
        super.onFirstViewAttach()
        viewState.startLoading()

        computersRepository.getComputers(page).compose(DBToUIComputerTransformer).async().subscribe({
            viewState.finishLoading()
            viewState.showComputers(it)
        }, {
            viewState.finishLoading()
        })
    }

    fun computerClicked(computerId: Int) = router.navigateTo(DETAIL_SCREEN, computerId)

}

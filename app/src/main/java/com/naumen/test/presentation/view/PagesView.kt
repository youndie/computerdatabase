package com.naumen.test.presentation.view

import com.arellomobile.mvp.MvpView

interface PagesView : MvpView {
    fun showPages(pages: IntArray)
    fun showError()
    fun startLoading()
    fun showPagesText(text: String)
}

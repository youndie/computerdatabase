package com.naumen.test.ui.fragment

import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.arellomobile.mvp.MvpFragment
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.naumen.test.R
import com.naumen.test.app.ComputersApp
import com.naumen.test.domain.repository.ToolbarTitleRepository
import com.naumen.test.presentation.models.UIComputer
import com.naumen.test.presentation.models.UIDetailComputer
import com.naumen.test.presentation.models.UILoader
import com.naumen.test.presentation.presenter.DetailPresenter
import com.naumen.test.presentation.view.DetailView
import com.naumen.test.ui.adapter.ComputerDetailAdapter
import kotlinx.android.synthetic.main.fragment_detail.*
import javax.inject.Inject

class DetailFragment : MvpFragment(), DetailView {
    var snackBar: Snackbar? = null

    override fun showLoadingError() {
        snackBar?.dismiss()
        snackBar = Snackbar.make(recyclerView, R.string.loading_error, Snackbar.LENGTH_INDEFINITE)
        snackBar?.show()
        snackBar?.setAction(R.string.retry, {
            snackBar?.dismiss()
            mDetailPresenter.loadComputer()
        })
    }

    @InjectPresenter
    lateinit var mDetailPresenter: DetailPresenter
    @Inject lateinit var toolbarTitleRepository: ToolbarTitleRepository

    init {
        ComputersApp.appComponent.inject(this)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_detail, container, false)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        recyclerView.adapter = adapter
        recyclerView.layoutManager = LinearLayoutManager(activity)
        recyclerView.recycledViewPool.setMaxRecycledViews(0, 0)
    }

    override fun finishLoadingSimilars() {
        adapter.data.remove(UILoader)
        adapter.notifyItemRemoved(1)
    }

    override fun showSimilars(it: Iterable<UIComputer>) {
        adapter.data.addAll(it)
        adapter.notifyItemRangeInserted(1, it.count())
    }

    override fun showDetailComputer(detailComputer: UIDetailComputer) {
        toolbarTitleRepository.setTitle(detailComputer.name)
        if (adapter.data.isEmpty()) {
            adapter.data.add(detailComputer)
            adapter.data.add(UILoader)
            adapter.notifyItemRangeInserted(0, 2)
        }
    }

    companion object {
        const val COMPUTER_ID_TAG = "computer:id:tag"
    }

    val adapter by lazy {
        ComputerDetailAdapter({ mDetailPresenter.loadSimilars() }, { mDetailPresenter.similarClicked(it) }, mvpDelegate)
    }

    @ProvidePresenter
    fun provideDetailPresenter() =
            DetailPresenter(arguments.getInt(COMPUTER_ID_TAG))


}

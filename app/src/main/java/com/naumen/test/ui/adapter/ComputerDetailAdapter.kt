package com.naumen.test.ui.adapter

import android.graphics.Bitmap
import android.support.v7.widget.RecyclerView
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.ViewGroup
import com.arellomobile.mvp.MvpDelegate
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.naumen.test.R
import com.naumen.test.common.configureWith
import com.naumen.test.presentation.models.UIComputer
import com.naumen.test.presentation.models.UIDetailComputer
import com.naumen.test.presentation.presenter.DetailHeaderPresenter
import com.naumen.test.presentation.view.DetailsHeaderView
import kotlinx.android.synthetic.main.computer_detail_header.view.*
import kotlinx.android.synthetic.main.similar_item.view.*


/**
 * Created by Pavel Votyakov on 03.05.17.
 * Work in progress © 2017
 */

class ComputerDetailAdapter(val similarLoadListener: () -> Unit, val similarClicked: (Int) -> Unit, val mvpDelegate: MvpDelegate<*>) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    val data = mutableListOf<Any>()

    val headerViewType = 0
    val similarViewType = 1
    val loaderViewType = 2


    override fun getItemViewType(position: Int) = when (data[position]) {
        is UIDetailComputer -> headerViewType
        is UIComputer -> similarViewType
        else -> {
            loaderViewType
        }
    }


    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder) {
            is HeaderViewHolder -> {
                holder.bind(data[position] as UIDetailComputer)
            }
            is SimilarViewHolder -> {
                val uiSimilar = data[position] as UIComputer
                holder.bind(uiSimilar)
                holder.itemView.setOnClickListener {
                    similarClicked.invoke(uiSimilar.id)
                }
            }
            is LoaderViewHolder -> {
                similarLoadListener.invoke()
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder? {
        val inflater = LayoutInflater.from(parent.context)

        return when (viewType) {
            headerViewType -> HeaderViewHolder(inflater.inflate(R.layout.computer_detail_header, parent, false), mvpDelegate)
            similarViewType -> SimilarViewHolder(inflater.inflate(R.layout.similar_item, parent, false))
            else -> {
                return LoaderViewHolder(inflater.inflate(R.layout.loader_item, parent, false))
            }
        }
    }

    override fun getItemCount() = data.size


}


class LoaderViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

}

class SimilarViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    fun bind(uiSimilar: UIComputer) {
        itemView.nameTextView.text = uiSimilar.name
    }

}

class HeaderViewHolder(itemView: View, val parentMvpDelegate: MvpDelegate<*>) : RecyclerView.ViewHolder(itemView), DetailsHeaderView {
    override fun hidePicture() {
        itemView.photoImageView.visibility = GONE
    }

    override fun hideProgress() {
        itemView.imageProgressBar.visibility = GONE

    }

    override fun showPicture(loadedImage: Bitmap) = with(itemView.photoImageView) {
        visibility = VISIBLE
        setImageBitmap(loadedImage)
        requestLayout()
    }

    override fun showProgress() {
        itemView.imageProgressBar.visibility = VISIBLE
    }

    override fun showFullText() {
        with(itemView.descriptionTextView) {
            maxLines = Integer.MAX_VALUE
            ellipsize = null
            requestLayout()
        }
    }

    override fun showDetailComputer(uiDetailComputer: UIDetailComputer) {
        with(itemView) {
            descriptionTextView.setOnClickListener { detailHeaderPresenter.textClicked() }
            companyNameTextView.configureWith(uiDetailComputer.companyName, companyNameHint)
            descriptionTextView.configureWith(uiDetailComputer.description, descriptionTextView)
            descriptionTextView.maxLines = 3
            descriptionTextView.ellipsize = TextUtils.TruncateAt.END
        }
    }

    @InjectPresenter
    lateinit var detailHeaderPresenter: DetailHeaderPresenter

    @ProvidePresenter
    fun provideDetailHeaderPresenter(): DetailHeaderPresenter {
        return DetailHeaderPresenter(uiDetailComputer)
    }

    private lateinit var uiDetailComputer: UIDetailComputer

    private var mvpDelegate: MvpDelegate<*>? = null

    fun bind(uiDetailComputer: UIDetailComputer) {
        if (mvpDelegate==null) {
            this.uiDetailComputer = uiDetailComputer

            mvpDelegate = MvpDelegate(this)
            mvpDelegate?.setParentDelegate(parentMvpDelegate, uiDetailComputer.id.toString())

            mvpDelegate?.onCreate()
            mvpDelegate?.onAttach()
        }
    }


}


package com.naumen.test.ui.fragment

import android.os.Bundle
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.ViewGroup
import com.arellomobile.mvp.MvpFragment
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.naumen.test.R
import com.naumen.test.app.ComputersApp
import com.naumen.test.presentation.models.UIComputer
import com.naumen.test.presentation.presenter.PagePresenter
import com.naumen.test.presentation.view.PageView
import com.naumen.test.ui.adapter.ComputersAdapter
import kotlinx.android.synthetic.main.fragment_page.*
import javax.inject.Inject

class PageFragment : MvpFragment(), PageView {

    @Inject
    lateinit var recycledViewPool: RecyclerView.RecycledViewPool

    init {
        ComputersApp.appComponent.inject(this)
    }

    private val adapter by lazy {
        ComputersAdapter {
            mPagePresenter.computerClicked(it)
        }
    }

    override fun showComputers(it: Iterable<UIComputer>) {
        adapter.computers.clear()
        adapter.computers.addAll(it)
        adapter.notifyDataSetChanged()
    }

    override fun finishLoading() {
        progressBar.visibility = GONE
        recyclerView.visibility = VISIBLE
    }


    override fun startLoading() {
        progressBar.visibility = VISIBLE
        recyclerView.visibility = GONE
    }


    @InjectPresenter
    lateinit var mPagePresenter: PagePresenter

    @ProvidePresenter
    fun providePagePresenter() = PagePresenter(arguments.getInt(PAGE_TAG))

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?) = inflater.inflate(R.layout.fragment_page, container, false)!!

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        recyclerView.recycledViewPool = recycledViewPool
        recyclerView.adapter = adapter
        recyclerView.layoutManager = LinearLayoutManager(activity)
        recyclerView.addItemDecoration(DividerItemDecoration(recyclerView.context, (recyclerView.layoutManager as LinearLayoutManager).orientation))
    }

    companion object {
        const val PAGE_TAG = "page:tag"
    }

}

package com.naumen.test.ui.activity

import android.app.Fragment
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.view.MenuItem
import com.arellomobile.mvp.MvpAppCompatActivity
import com.arellomobile.mvp.presenter.InjectPresenter
import com.naumen.test.R
import com.naumen.test.app.ComputersApp
import com.naumen.test.app.DETAIL_SCREEN
import com.naumen.test.app.PAGES_SCREEN
import com.naumen.test.common.FragmentNavigator
import com.naumen.test.presentation.presenter.MainPresenter
import com.naumen.test.presentation.view.MainView
import com.naumen.test.ui.fragment.DetailFragment
import com.naumen.test.ui.fragment.PagesFragment
import kotlinx.android.synthetic.main.activity_main.*
import ru.terrakok.cicerone.NavigatorHolder
import ru.terrakok.cicerone.commands.Back
import ru.terrakok.cicerone.commands.BackTo
import ru.terrakok.cicerone.commands.Command
import ru.terrakok.cicerone.commands.Replace
import javax.inject.Inject


class MainActivity : MvpAppCompatActivity(), MainView {
    override fun configureToolbar(backVisible: Boolean) {
        supportActionBar?.setDisplayHomeAsUpEnabled(backVisible)
    }

    override fun showTitle(title: String) {
        this.title = title
    }

    @Inject
    lateinit var navigationHolder: NavigatorHolder

    @InjectPresenter
    lateinit var mMainPresenter: MainPresenter

    var snackbar: Snackbar? = null
    val navigator: FragmentNavigator by lazy {
        object : FragmentNavigator(fragmentManager, R.id.mainContainer) {
            override fun createFragment(screenKey: String, data: Any?): Fragment {
                return when (screenKey) {
                    PAGES_SCREEN -> PagesFragment()
                    DETAIL_SCREEN -> {
                        val detailFragment = DetailFragment()
                        val args = Bundle()
                        args.putInt(DetailFragment.COMPUTER_ID_TAG, data!! as Int)
                        detailFragment.arguments = args
                        detailFragment
                    }

                    else -> {
                        Fragment()
                    }
                }

            }

            override fun exit() {
                finish()
            }

            override fun showSystemMessage(message: String) {
                snackbar?.dismiss()
                snackbar = Snackbar.make(mainCoordinator, message, Snackbar.LENGTH_LONG)
                snackbar?.show()
            }

            override fun applyCommand(command: Command) {
                mMainPresenter.onCommand(command)
                super.applyCommand(command)

            }
        }
    }

    override fun onBackPressed() {
        navigator.applyCommand(Back())
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item?.itemId == android.R.id.home) {
            navigator.applyCommand(BackTo(PAGES_SCREEN))
            mMainPresenter.resetBackStack()
            return true
        }

        return super.onOptionsItemSelected(item)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        ComputersApp.appComponent.inject(this)
        setContentView(R.layout.activity_main)
        if (savedInstanceState == null) {
            navigator.applyCommand(Replace(PAGES_SCREEN, null))
        }
    }

    override fun onResume() {
        super.onResume()
        navigationHolder.setNavigator(navigator)
    }

    override fun onPause() {
        super.onPause()
        navigationHolder.removeNavigator()
    }

}

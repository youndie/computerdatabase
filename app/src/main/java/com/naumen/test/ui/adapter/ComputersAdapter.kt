package com.naumen.test.ui.adapter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.naumen.test.R
import com.naumen.test.common.configureWith
import com.naumen.test.presentation.models.UIComputer
import kotlinx.android.synthetic.main.computer_item.view.*

/**
 * Created by Pavel Votyakov on 03.05.17.
 * Work in progress © 2017
 */

class ComputersAdapter(val listener: (Int) -> Unit) : RecyclerView.Adapter<ComputerViewHolder>() {

    val computers = mutableListOf<UIComputer>()

    override fun onBindViewHolder(holder: ComputerViewHolder, position: Int)
            = holder.bind(computers[position], listener)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int)
            = ComputerViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.computer_item, parent, false))

    override fun getItemCount() = computers.size

}

class ComputerViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    val nameTextView: TextView by lazy { itemView.nameTextView }
    val companyNameTextView: TextView by lazy { itemView.companyNameTextView }
    val companyNameHint: TextView by lazy { itemView.companyNameHint }

    fun bind(uiComputer: UIComputer, listener: (Int) -> Unit) {
        itemView.setOnClickListener {
            listener.invoke(uiComputer.id)
        }
        nameTextView.text = uiComputer.name
        companyNameTextView.configureWith(uiComputer.companyName, companyNameHint)
    }


}


package com.naumen.test.ui.adapter

import android.app.Fragment
import android.app.FragmentManager
import android.os.Bundle
import android.support.v13.app.FragmentPagerAdapter
import com.naumen.test.ui.fragment.PageFragment


/**
 * Created by Pavel Votyakov on 03.05.17.
 * Work in progress © 2017
 */

class PagesAdapter(fm: FragmentManager, val pages: IntArray) : FragmentPagerAdapter(fm) {


    override fun getItem(position: Int): Fragment {
        val fragment = PageFragment()
        val args = Bundle()
        args.putInt(PageFragment.PAGE_TAG, pages[position])
        fragment.arguments = args
        return fragment
    }

    override fun getCount() = pages.size

}
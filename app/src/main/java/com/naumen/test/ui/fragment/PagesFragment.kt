package com.naumen.test.ui.fragment

import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v4.view.ViewPager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.arellomobile.mvp.MvpFragment
import com.arellomobile.mvp.presenter.InjectPresenter
import com.naumen.test.R
import com.naumen.test.presentation.presenter.PagesPresenter
import com.naumen.test.presentation.view.PagesView
import com.naumen.test.ui.adapter.PagesAdapter
import kotlinx.android.synthetic.main.fragment_pages.*

class PagesFragment : MvpFragment(), PagesView {
    override fun showPagesText(text: String) {
        pagesTextView.text = text
    }

    override fun showPages(pages: IntArray) {
        viewPager.removeOnPageChangeListener(pageListener)
        viewPager.adapter = PagesAdapter(childFragmentManager, pages)
        viewPager.addOnPageChangeListener(pageListener)
    }

    val pageListener: ViewPager.OnPageChangeListener by lazy {
        object : ViewPager.OnPageChangeListener {
            override fun onPageScrollStateChanged(state: Int) {

            }

            override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {

            }

            override fun onPageSelected(position: Int) {
                leftButton.isEnabled = position != 0
                rightButton.isEnabled = position != viewPager.adapter.count - 1
                mPagesPresenter.onCurrentPageChanges(position)
            }

        }
    }


    var snackbar: Snackbar? = null

    override fun startLoading() {
        snackbar?.dismiss()
    }

    override fun showError() {
        snackbar = Snackbar.make(viewPager, R.string.loading_error, Snackbar.LENGTH_INDEFINITE)
        snackbar?.setAction(R.string.retry, {
            mPagesPresenter.load()
        })
        snackbar?.show()
    }

    companion object {
        const val TAG = "PagesFragment"
    }

    @InjectPresenter
    lateinit var mPagesPresenter: PagesPresenter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_pages, container, false)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewPager.offscreenPageLimit = 5
        leftButton.setOnClickListener { viewPager.currentItem-- }
        rightButton.setOnClickListener { viewPager.currentItem++ }
    }
}

package com.naumen.test.di.modules

import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import com.naumen.test.app.SERVER_NAME
import dagger.Module
import dagger.Provides
import io.reactivex.schedulers.Schedulers
import okhttp3.OkHttpClient
import retrofit2.Converter
import retrofit2.Retrofit
import javax.inject.Singleton


/**
 * Created by Pavel Votyakov on 28.04.17.
 * Work in progress © 2017
 */

@Module(includes = arrayOf(GsonModule::class, OkHttpClientModule::class))
class RetrofitModule {

    @Provides
    @Singleton
    fun provideRetrofit(builder: Retrofit.Builder) =
            builder.baseUrl(SERVER_NAME).build()!!


    @Provides
    @Singleton
    fun provideRetrofitBuilder(converterFactory: Converter.Factory, okHttpClient: OkHttpClient)
            = Retrofit.Builder()
            .client(okHttpClient)
            .addCallAdapterFactory(RxJava2CallAdapterFactory.createWithScheduler(Schedulers.io()))
            .addConverterFactory(converterFactory)!!


}
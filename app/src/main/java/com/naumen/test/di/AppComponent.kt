package com.naumen.test.di

import android.content.Context
import com.naumen.test.di.common.RecycledViewPool
import com.naumen.test.di.modules.ComputersRepositoryModule
import com.naumen.test.di.modules.ContextModule
import com.naumen.test.di.modules.NavigationModule
import com.naumen.test.di.modules.ToolbarTitleModule
import com.naumen.test.presentation.presenter.DetailPresenter
import com.naumen.test.presentation.presenter.MainPresenter
import com.naumen.test.presentation.presenter.PagePresenter
import com.naumen.test.presentation.presenter.PagesPresenter
import com.naumen.test.ui.activity.MainActivity
import com.naumen.test.ui.fragment.DetailFragment
import com.naumen.test.ui.fragment.PageFragment
import dagger.Component
import javax.inject.Singleton

/**
 * Created by Pavel Votyakov on 28.04.17.
 * Work in progress © 2017
 */

@Singleton
@Component(modules = arrayOf(ContextModule::class, ComputersRepositoryModule::class, RecycledViewPool::class, NavigationModule::class, ToolbarTitleModule::class))
interface AppComponent {
    fun getContext(): Context

    fun inject(pageFragment: PageFragment)
    fun inject(mainActivity: MainActivity)
    fun inject(detailFragment: DetailFragment)

    fun inject(pagePresenter: PagePresenter)
    fun inject(computerDetailPresenter: DetailPresenter)
    fun inject(pagesPresenter: PagesPresenter)
    fun inject(mainPresenter: MainPresenter)

}
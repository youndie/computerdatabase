package com.naumen.test.di.modules

import com.naumen.test.domain.repository.ToolbarTitleRepository
import dagger.Module
import dagger.Provides
import io.reactivex.subjects.BehaviorSubject
import javax.inject.Singleton

/**
 * Created by Pavel Votyakov on 05.05.17.
 * Work in progress © 2017
 */

@Module
class ToolbarTitleModule {

    @Provides
    @Singleton
    fun provideToolbarTitleRepository(): ToolbarTitleRepository = ToolbarTitleRepository(BehaviorSubject.create<String>())

}
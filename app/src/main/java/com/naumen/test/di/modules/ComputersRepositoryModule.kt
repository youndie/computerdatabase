package com.naumen.test.di.modules

import com.naumen.test.domain.repository.ComputersRepository
import com.naumen.test.domain.service.ComputersDBService
import com.naumen.test.domain.service.ComputersNWService
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

/**
 * Created by Pavel Votyakov on 03.05.17.
 * Work in progress © 2017
 */
@Module(includes = arrayOf(ComputersDBModule::class, ComputersNWModule::class))
class ComputersRepositoryModule {

    @Provides
    @Singleton
    fun providesComputersRepository(computersDBService: ComputersDBService, computersNWService: ComputersNWService)
            = ComputersRepository(network = computersNWService, db = computersDBService)

}
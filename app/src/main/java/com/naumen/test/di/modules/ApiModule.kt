package com.naumen.test.di.modules

import com.naumen.test.di.modules.RetrofitModule
import com.naumen.test.app.ComputersApi
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import javax.inject.Singleton

/**
 * Created by Pavel Votyakov on 28.04.17.
 * Work in progress © 2017
 */


@Module(includes = arrayOf(RetrofitModule::class))
class ApiModule {

    @Provides
    @Singleton
    fun provideComputersApi(retrofit: Retrofit): ComputersApi = retrofit.create(ComputersApi::class.java)
}


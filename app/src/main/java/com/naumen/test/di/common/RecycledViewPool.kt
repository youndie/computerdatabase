package com.naumen.test.di.common

import android.support.v7.widget.RecyclerView
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

/**
 * Created by Pavel Votyakov on 03.05.17.
 * Work in progress © 2017
 */
@Module
class RecycledViewPool {

    @Provides
    @Singleton
    fun provideViewPool(): RecyclerView.RecycledViewPool {
        val recycledViewPool = RecyclerView.RecycledViewPool()
        return recycledViewPool
    }
}

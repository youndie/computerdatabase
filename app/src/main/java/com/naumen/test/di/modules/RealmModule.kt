package com.naumen.test.di.modules

import android.content.Context
import com.naumen.test.domain.service.RealmService
import dagger.Module
import dagger.Provides
import io.realm.Realm
import javax.inject.Singleton

/**
 * Created by Pavel Votyakov on 03.05.17.
 * Work in progress © 2017
 */
@Module
class RealmModule {

    @Provides
    @Singleton
    fun provideRealmService(context: Context): RealmService {
        Realm.init(context)
        return RealmService()
    }

}
package com.naumen.test.di.modules

import com.naumen.test.app.ComputersApi
import com.naumen.test.domain.service.ComputersNWService
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

/**
 * Created by Pavel Votyakov on 28.04.17.
 * Work in progress © 2017
 */

@Module(includes = arrayOf(ApiModule::class))
class ComputersNWModule {

    @Provides
    @Singleton
    fun provideComputersNWService(computersApi: ComputersApi) = ComputersNWService(computersApi)

}
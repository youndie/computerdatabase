package com.naumen.test.di.modules

import com.naumen.test.domain.service.ComputersDBService
import com.naumen.test.domain.service.RealmService
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

/**
 * Created by Pavel Votyakov on 03.05.17.
 * Work in progress © 2017
 */

@Module(includes = arrayOf(RealmModule::class))
class ComputersDBModule {

    @Provides
    @Singleton
    fun providesComputersDBService(realmProvider: RealmService) = ComputersDBService(realmProvider)

}
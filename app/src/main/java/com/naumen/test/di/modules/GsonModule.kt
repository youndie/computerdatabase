package com.naumen.test.di.modules

import com.google.gson.Gson
import com.google.gson.GsonBuilder
import dagger.Module
import dagger.Provides
import retrofit2.Converter
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

/**
 * Created by Pavel Votyakov on 28.04.17.
 * Work in progress © 2017
 */
@Module
class GsonModule {

    @Provides
    @Singleton
    fun provideConverterFactory(gson: Gson): Converter.Factory
            = GsonConverterFactory.create(gson)

    @Provides
    @Singleton
    fun provideGson()
            = GsonBuilder().create()!!


}
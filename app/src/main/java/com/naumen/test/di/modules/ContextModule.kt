package com.naumen.test.di.modules

import android.content.Context
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

/**
 * Created by Pavel Votyakov on 28.04.17.
 * Work in progress © 2017
 */
@Module
class ContextModule(val context: Context) {

    @Singleton
    @Provides
    fun provideContext() = context

}
package com.naumen.test.common

import io.reactivex.*
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

/**
 * Created by Pavel Votyakov on 07.04.17.
 * Work in progress © 2017
 */

fun <T> Observable<T>.async(): Observable<T> = subscribeOn(RxSchedulers.io()).observeOn(RxSchedulers.main())

fun <T> Flowable<T>.async(): Flowable<T> = subscribeOn(RxSchedulers.io()).observeOn(RxSchedulers.main())

fun <T> Single<T>.async(): Single<T> = subscribeOn(RxSchedulers.io()).observeOn(RxSchedulers.main())

fun <T> Maybe<T>.async(): Maybe<T> = subscribeOn(RxSchedulers.io()).observeOn(RxSchedulers.main())
fun <T> Maybe<T>.main(): Maybe<T> = subscribeOn(RxSchedulers.main()).observeOn(RxSchedulers.main())

fun Completable.async(): Completable = subscribeOn(RxSchedulers.io()).observeOn(RxSchedulers.main())


object RxSchedulers {

    fun io(): Scheduler {
        return Schedulers.io()
    }

    fun main(): Scheduler {
        return AndroidSchedulers.mainThread()
    }
}
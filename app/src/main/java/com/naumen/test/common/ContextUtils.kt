package com.naumen.test.common

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.Parcelable
import java.io.Serializable

/**
 * Created by Pavel Votyakov on 03.05.17.
 * Work in progress © 2017
 */


inline fun <reified T : Any> Context.intentFor(): Intent = Intent(this, T::class.java)

inline fun <reified T : Any> Context.startActivity() {
    startActivity(this.intentFor<T>())
}

inline fun <reified T : Any> Activity.startActivityForResult(requestCode: Int) {
    startActivityForResult(this.intentFor<T>(), requestCode)
}

inline fun <reified T : Any> Context.startActivity(vararg params: Pair<String, Any>) {
    startActivity(fillIntentArguments(this.intentFor<T>(), params))
}


fun fillIntentArguments(intent: Intent, params: Array<out Pair<String, Any?>>): Intent {

    params.forEach {
        val value = it.second
        when (value) {
            null -> intent.putExtra(it.first, null as Serializable?)
            is Int -> intent.putExtra(it.first, value)
            is Long -> intent.putExtra(it.first, value)
            is CharSequence -> intent.putExtra(it.first, value)
            is String -> intent.putExtra(it.first, value)
            is Float -> intent.putExtra(it.first, value)
            is Double -> intent.putExtra(it.first, value)
            is Char -> intent.putExtra(it.first, value)
            is Short -> intent.putExtra(it.first, value)
            is Boolean -> intent.putExtra(it.first, value)
            is Serializable -> intent.putExtra(it.first, value)
            is Bundle -> intent.putExtra(it.first, value)
            is Parcelable -> intent.putExtra(it.first, value)
            is Array<*> -> when {
                value.isArrayOf<CharSequence>() -> intent.putExtra(it.first, value)
                value.isArrayOf<String>() -> intent.putExtra(it.first, value)
                value.isArrayOf<Parcelable>() -> intent.putExtra(it.first, value)
            }
            is IntArray -> intent.putExtra(it.first, value)
            is LongArray -> intent.putExtra(it.first, value)
            is FloatArray -> intent.putExtra(it.first, value)
            is DoubleArray -> intent.putExtra(it.first, value)
            is CharArray -> intent.putExtra(it.first, value)
            is ShortArray -> intent.putExtra(it.first, value)
            is BooleanArray -> intent.putExtra(it.first, value)
        }
        return@forEach
    }
    return intent
}

package com.naumen.test.common

import android.view.View.GONE
import android.view.View.VISIBLE
import android.widget.TextView

/**
 * Created by Pavel Votyakov on 03.05.17.
 * Work in progress © 2017
 */

fun TextView.configureWith(text: String?, hintTextView: TextView) {
    if (text.isNullOrEmpty()) {
        visibility = GONE
        hintTextView.visibility = GONE
    } else {
        visibility = VISIBLE
        hintTextView.visibility = VISIBLE
    }
    this.text = text
}
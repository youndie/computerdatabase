package com.naumen.test.domain.service

import com.naumen.test.app.ComputersApi

/**
 * Created by Pavel Votyakov on 28.04.17.
 * Work in progress © 2017
 */

class ComputersNWService(private val computersApi: ComputersApi) {

    fun getComputers(page: Int) = computersApi.getComputers(page)
    fun getDetailComputer(id: Int) = computersApi.getComputerById(id)
    fun getSimilars(id:Int) = computersApi.getSimilars(id)

}
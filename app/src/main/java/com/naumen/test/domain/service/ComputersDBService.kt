package com.naumen.test.domain.service

import com.naumen.test.domain.models.DBComputer
import com.naumen.test.domain.models.DBDetailComputer
import io.reactivex.Maybe

/**
 * Created by Pavel Votyakov on 03.05.17.
 * Work in progress © 2017
 */

class ComputersDBService(val realmProvider: RealmService) {

    fun saveComputers(computers: List<DBComputer>): Maybe<List<DBComputer>> =
            realmProvider.queryListToRealm { it.copyToRealmOrUpdate(computers) }

    fun loadComputers(page: Int): Maybe<List<DBComputer>> =
            realmProvider.queryListFromRealm {
                it.where(DBComputer::class.java).equalTo("page", page).findAll()
            }

    fun loadComputers() =
            realmProvider.queryListFromRealm {
                it.where(DBComputer::class.java).findAll()
            }

    fun saveDetailComputer(detailComputer: DBDetailComputer): Maybe<DBDetailComputer> =
            realmProvider.queryFromRealm { it.copyToRealmOrUpdate(detailComputer) }

    fun loadDetailComputer(id: Int): Maybe<DBDetailComputer> {
        return realmProvider.queryFromRealm { it.where(DBDetailComputer::class.java).equalTo("id", id).findFirst() }
    }

}
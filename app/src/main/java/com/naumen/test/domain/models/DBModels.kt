package com.naumen.test.domain.models

import io.realm.RealmModel
import io.realm.annotations.PrimaryKey
import io.realm.annotations.RealmClass
import java.util.*

/**
 * Created by Pavel Votyakov on 03.05.17.
 * Work in progress © 2017
 */
@RealmClass
open class DBComputer : RealmModel {
    @PrimaryKey
    open var id = 0
    open var name = ""
    open var page = 0
    open var company: DBCompany? = null
}

@RealmClass
open class DBDetailComputer : RealmModel {
    @PrimaryKey
    open var id = 0
    open var name = ""
    open var page = 0
    open var company: DBCompany? = null
    open var imageUrl: String? = null
    open var description: String? = null

    open var created: Date = Date()
}

@RealmClass
open class DBCompany : RealmModel {
    @PrimaryKey
    open var id = 0
    open var name = ""
}
package com.naumen.test.domain.transformer

import com.naumen.test.domain.models.*

/**
 * Created by Pavel Votyakov on 03.05.17.
 * Work in progress © 2017
 */

object NWToDBComputerTransformer {

    fun transform(networkComputer: NWComputer, page: Int): DBComputer {
        val dbComputer = DBComputer()
        dbComputer.id = networkComputer.id
        dbComputer.name = networkComputer.name
        dbComputer.page = page

        if (networkComputer.company != null) {
            val dbCompany = DBCompany()
            dbCompany.id = networkComputer.company.id
            dbCompany.name = networkComputer.company.name
            dbComputer.company = dbCompany
        }


        return dbComputer
    }

    fun transform(netNWDetailComputer: NWDetailComputer): DBDetailComputer {
        val dbDetailComputer = DBDetailComputer()

        dbDetailComputer.id = netNWDetailComputer.id
        dbDetailComputer.name = netNWDetailComputer.name

        if (netNWDetailComputer.company != null) {
            val dbCompany = DBCompany()
            dbCompany.id = netNWDetailComputer.company.id
            dbCompany.name = netNWDetailComputer.company.name
            dbDetailComputer.company = dbCompany
        }
        dbDetailComputer.description = netNWDetailComputer.description
        dbDetailComputer.imageUrl = netNWDetailComputer.imageUrl

        return dbDetailComputer
    }


}

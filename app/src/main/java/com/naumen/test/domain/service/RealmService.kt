package com.naumen.test.domain.service

import io.reactivex.Maybe
import io.realm.Realm
import io.realm.RealmModel
import io.realm.RealmObject
import io.realm.RealmResults
import java.util.*

/**
 * Created by Pavel Votyakov on 03.05.17.
 * Work in progress © 2017
 */


class RealmService {

    fun <T : RealmModel> queryFromRealm(func: (Realm) -> T?): Maybe<T> {
        val realm = Realm.getDefaultInstance()

        return Maybe.defer<T> {
            try {
                realm.beginTransaction()
                val result = func(realm)
                var resultCloned: T? = null
                if (RealmObject.isValid(result) && result != null)
                    resultCloned = realm.copyFromRealm(result)
                realm.commitTransaction()

                if (resultCloned == null) {
                    return@defer Maybe.error(NoSuchElementException())
                } else
                    return@defer Maybe.just <T>(resultCloned)
            } catch (e: Exception) {
                realm.cancelTransaction()
                Maybe.error<T>(Exception("Something went wrong", e))
            } finally {
                realm.close()
            }
        }
    }

    fun <T : RealmModel> queryListToRealm(func: (Realm) -> List<T>): Maybe<List<T>> {
        val realm = Realm.getDefaultInstance()

        return Maybe.defer<List<T>> {
            try {
                realm.beginTransaction()
                val result = func(realm)
                val resultCloned = result.map { realm.copyFromRealm(it) }
                realm.commitTransaction()
                return@defer Maybe.just<List<T>>(resultCloned)
            } catch (e: Exception) {
                realm.cancelTransaction()
                Maybe.error<List<T>>(Exception("Something went wrong", e))
            } finally {
                realm.close()
            }
        }
    }

    fun <T : RealmModel> queryListFromRealm(func: (Realm) -> RealmResults<T>): Maybe<List<T>> {
        val realm = Realm.getDefaultInstance()

        return Maybe.defer<List<T>> {
            try {
                realm.beginTransaction()
                val result = func(realm)
                val resultCloned = result.map { realm.copyFromRealm(it) }
                realm.commitTransaction()
                return@defer Maybe.just<List<T>>(resultCloned)
            } catch (e: Exception) {
                realm.cancelTransaction()
                Maybe.error<List<T>>(Exception("Something went wrong", e))
            } finally {
                realm.close()
            }
        }
    }
}
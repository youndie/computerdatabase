package com.naumen.test.domain.models

/**
 * Created by Pavel Votyakov on 03.05.17.
 * Work in progress © 2017
 */

class NWCompany(val id: Int, val name: String)

class NWComputer(val id: Int, val name: String, val company: NWCompany?)

class NWComputersResponse(val items: Array<NWComputer>, val page: Int, val offset: Int, val total: Int)

class NWDetailComputer(val id: Int, val name: String, val imageUrl: String?, val company: NWCompany?, val description: String?)

class NWSimilarComputer(val id: Int, val name: String)
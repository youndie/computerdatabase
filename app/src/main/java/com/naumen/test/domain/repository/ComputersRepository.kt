package com.naumen.test.domain.repository

import com.naumen.test.common.async
import com.naumen.test.common.main
import com.naumen.test.domain.models.DBComputer
import com.naumen.test.domain.models.DBDetailComputer
import com.naumen.test.domain.service.ComputersDBService
import com.naumen.test.domain.service.ComputersNWService
import com.naumen.test.domain.transformer.NWToDBComputerTransformer
import io.reactivex.Maybe
import java.util.*

/**
 * Created by Pavel Votyakov on 03.05.17.
 * Work in progress © 2017
 */

class ComputersRepository(val network: ComputersNWService, val db: ComputersDBService) {

    fun getPages(): Maybe<IntArray> {
        return network.getComputers(0).async().map { (0..(it.total / it.items.size) + 1).toList().toIntArray() }
                .onErrorResumeNext(db.loadComputers().main().map { it.map { it.page }.distinct().toIntArray() }
                )
    }

    fun getComputers(page: Int): Maybe<List<DBComputer>> {
        return network.getComputers(page).async().flatMap {
            response ->
            db.saveComputers(response.items.map { NWToDBComputerTransformer.transform(it, response.page) }).main()
        }.onErrorResumeNext(db.loadComputers(page).main())
    }

    fun getDetailComputer(id: Int): Maybe<DBDetailComputer> =
            db.loadDetailComputer(id).main().onErrorResumeNext(
                    network.getDetailComputer(id).async().flatMap {
                        db.saveDetailComputer(NWToDBComputerTransformer.transform(it)).main()
                    }
            ).filter { Math.abs(Date().time - it.created.time) < 1000 * 60 }.switchIfEmpty {
                network.getDetailComputer(id).async().flatMap {
                    db.saveDetailComputer(NWToDBComputerTransformer.transform(it)).main()
                }.onErrorResumeNext(db.loadDetailComputer(id).main())
            }

    fun getSimilars(id: Int) = network.getSimilars(id).async()
}
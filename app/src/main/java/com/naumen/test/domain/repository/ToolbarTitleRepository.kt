package com.naumen.test.domain.repository

import io.reactivex.subjects.BehaviorSubject

/**
 * Created by Pavel Votyakov on 05.05.17.
 * Work in progress © 2017
 */

class ToolbarTitleRepository(private val subject: BehaviorSubject<String>) {
    fun subscribe(subs: (String) -> Unit) = subject.subscribe(subs)
    fun setTitle(title: String) = subject.onNext(title)
}
